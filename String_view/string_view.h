#ifndef STRING_VIEW_H
#define STRING_VIEW_H

#include <stdexcept>

class StringViewOutOfRange : public std::out_of_range {
 public:
  StringViewOutOfRange() : std::out_of_range("StringViewOutOfRange") {
  }
};

class StringView {
 public:
  StringView();
  StringView(const char* str);  // NOLINT
  StringView(const char* str, size_t n);
  size_t Size() const;
  size_t Length() const;
  bool Empty() const;
  const char& Front() const;
  const char& Back() const;
  const char* Data() const;
  void Swap(StringView& other);
  const char& At(size_t idx) const;
  const char& operator[](size_t idx) const;
  void RemovePrefix(size_t prefix_size);
  void RemoveSuffix(size_t suffix_size);
  StringView Substr(size_t pos, size_t count = -1);

 private:
  const char* str_;
  size_t n_;
};

#endif