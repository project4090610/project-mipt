#include "string_view.h"
#include <stdexcept>
#include <iostream>
#include <cstring>

StringView::StringView() : str_(nullptr), n_(0) {
}

StringView::StringView(const char* str) : str_(str), n_(std::strlen(str)) {
}

StringView::StringView(const char* str, size_t n) : str_(str), n_(n) {
}

size_t StringView::Size() const {
  return n_;
}

size_t StringView::Length() const {
  return n_;
}

bool StringView::Empty() const {
  return n_ == 0;
}

void StringView::Swap(StringView& other) {
  auto temp_str = other.str_;
  other.str_ = str_;
  str_ = temp_str;
  auto temp_n = other.n_;
  other.n_ = n_;
  n_ = temp_n;
}

const char& StringView::Front() const {
  return str_[0];
}

const char& StringView::Back() const {
  return str_[n_ - 1];
}

const char* StringView::Data() const {
  return str_;
}

const char& StringView::operator[](size_t idx) const {
  return *(str_ + idx);
}

const char& StringView::At(size_t idx) const {
  if (idx >= n_) {
    throw StringViewOutOfRange{};
  }
  return *(str_ + idx);
}

void StringView::RemovePrefix(size_t prefix_size) {
  for (size_t i = 0; i < prefix_size; ++i) {
    ++str_;
    --n_;
  }
}

void StringView::RemoveSuffix(size_t suffix_size) {
  for (size_t i = 0; i < suffix_size; ++i) {
    --n_;
  }
}

StringView StringView::Substr(size_t pos, size_t count) {
  if (pos >= Size()) {
    throw StringViewOutOfRange{};
  }
  StringView string(str_ + pos, std::min(count, Size() - pos));
  return {string};
}
