#ifndef UNIQUE_PTR_H
#define UNIQUE_PTR_H

template <class T>
class UniquePtr {
  T* ptr_ = nullptr;

 public:
  UniquePtr() = default;

  explicit UniquePtr(T* pointer) : ptr_(pointer) {
  }

  UniquePtr(UniquePtr&& other) noexcept : ptr_(other.ptr_) {
    other.ptr_ = nullptr;
  }

  UniquePtr(const UniquePtr& other) = delete;

  UniquePtr& operator=(const UniquePtr& other) = delete;

  UniquePtr& operator=(UniquePtr&& other) noexcept {
    if (this != &other) {
      Reset(other.Release());
    }
    return *this;
  }

  T* Get() const {
    return ptr_;
  }

  T& operator*() const {
    return *ptr_;
  }

  ~UniquePtr() {
    delete ptr_;
  }

  T* operator->() const {
    return ptr_;
  }

  void Reset(T* pointer = nullptr) {
    delete ptr_;
    ptr_ = pointer;
  }

  T* Release() {
    auto pointer = ptr_;
    ptr_ = nullptr;
    return pointer;
  }

  void Swap(UniquePtr& other) {
    std::swap(ptr_, other.ptr_);
  }

  explicit operator bool() const {
    return ptr_ != nullptr;
  }
};

#endif