#ifndef RATIONAL_H
#define RATIONAL_H

#include <stdexcept>
#include <iostream>
#include <cmath>
#include <stdint.h>
#include <utility>

class RationalDivisionByZero : public std::runtime_error {
 public:
  RationalDivisionByZero() : std::runtime_error("RationalDivisionByZero") {
  }
};

class Rational {
 public:
  Rational(int lhs = 0, int rhs = 1);  // NOLINT
  int GetNumerator() const;
  int GetDenominator() const;
  void SetNumerator(int numerator);
  void SetDenominator(int denominator);
  void NormalRational();

 private:
  int numerator_;
  int denominator_;
};

Rational operator+(const Rational& lhs, const Rational& rhs);
Rational operator-(const Rational& lhs, const Rational& rhs);
Rational operator*(const Rational& lhs, const Rational& rhs);
Rational operator/(const Rational& lhs, const Rational& rhs);
Rational& operator+=(Rational& lhs, const Rational& rhs);
Rational& operator-=(Rational& lhs, const Rational& rhs);
Rational& operator*=(Rational& lhs, const Rational& rhs);
Rational& operator/=(Rational& lhs, const Rational& rhs);
Rational operator-(const Rational& rational);
Rational operator+(const Rational& rational);
Rational& operator--(Rational& rational);
Rational& operator++(Rational& rational);
Rational operator--(Rational& rational, int);
Rational operator++(Rational& rational, int);
bool operator>(const Rational& lhs, const Rational& rhs);
bool operator<(const Rational& lhs, const Rational& rhs);
bool operator==(const Rational& lhs, const Rational& rhs);
bool operator!=(const Rational lhs, const Rational rhs);
bool operator<=(const Rational lhs, const Rational rhs);
bool operator>=(const Rational lhs, const Rational rhs);

std::ostream& operator<<(std::ostream& os, const Rational& rational);
std::istream& operator>>(std::istream& is, Rational& rational);

#endif