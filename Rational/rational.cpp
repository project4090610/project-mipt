#include "rational.h"
#include <stdexcept>
#include <iostream>
#include <cmath>
#include <numeric>
#include <cstdint>
#include <utility>

Rational::Rational(int lhs, int rhs) : numerator_(lhs), denominator_(rhs) {
  if (denominator_ == 0) {
    throw RationalDivisionByZero{};
  }
  NormalRational();
}

void Rational::NormalRational() {
  const auto gcd = std::gcd(numerator_, denominator_);
  numerator_ /= gcd;
  denominator_ /= gcd;
  if (denominator_ < 0) {
    numerator_ = -numerator_;
    denominator_ = -denominator_;
  }
}

int Rational::GetNumerator() const {
  return numerator_;
}

int Rational::GetDenominator() const {
  return denominator_;
}

void Rational::SetNumerator(int numerator) {
  numerator_ = numerator;
  NormalRational();
}

void Rational::SetDenominator(int denominator) {
  if (denominator == 0) {
    throw RationalDivisionByZero{};
  }
  denominator_ = denominator;
  NormalRational();
}

Rational operator+(const Rational& lhs, const Rational& rhs) {
  int32_t numerator = lhs.GetNumerator() * rhs.GetDenominator() + rhs.GetNumerator() * lhs.GetDenominator();
  int32_t denominator = lhs.GetDenominator() * rhs.GetDenominator();
  Rational rational_new(numerator, denominator);
  rational_new.NormalRational();
  return rational_new;
}

Rational operator-(const Rational& lhs, const Rational& rhs) {
  Rational rational_new;
  if (lhs.GetDenominator() != rhs.GetDenominator()) {
    int32_t numerator = lhs.GetNumerator() * rhs.GetDenominator() - rhs.GetNumerator() * lhs.GetDenominator();
    int32_t denominator = lhs.GetDenominator() * rhs.GetDenominator();
    rational_new.SetNumerator(numerator);
    rational_new.SetDenominator(denominator);
  } else if (lhs.GetDenominator() == rhs.GetDenominator()) {
    int32_t numerator = lhs.GetNumerator() - rhs.GetNumerator();
    int32_t denominator = lhs.GetDenominator();
    rational_new.SetNumerator(numerator);
    rational_new.SetDenominator(denominator);
  }
  rational_new.NormalRational();
  return rational_new;
}

Rational operator*(const Rational& lhs, const Rational& rhs) {
  int32_t numerator = lhs.GetNumerator() * rhs.GetNumerator();
  int32_t denominator = lhs.GetDenominator() * rhs.GetDenominator();
  Rational rational_new(numerator, denominator);
  rational_new.NormalRational();
  return rational_new;
}

Rational operator/(const Rational& lhs, const Rational& rhs) {
  int32_t numerator = lhs.GetNumerator() * rhs.GetDenominator();
  int32_t denominator = lhs.GetDenominator() * rhs.GetNumerator();
  Rational rational_new(numerator, denominator);
  rational_new.NormalRational();
  return rational_new;
}

Rational& operator+=(Rational& lhs, const Rational& rhs) {
  Rational& rational = lhs;
  rational = rational + rhs;
  rational.NormalRational();
  return rational;
}

Rational& operator-=(Rational& lhs, const Rational& rhs) {
  Rational& rational = lhs;
  rational = rational - rhs;
  rational.NormalRational();
  return rational;
}

Rational& operator*=(Rational& lhs, const Rational& rhs) {
  Rational& rational = lhs;
  rational = rational * rhs;
  rational.NormalRational();
  return rational;
}

Rational& operator/=(Rational& lhs, const Rational& rhs) {
  Rational& rational = lhs;
  rational = rational / rhs;
  rational.NormalRational();
  return rational;
}

Rational operator-(const Rational& rational) {
  int32_t numerator = -rational.GetNumerator();
  int32_t denominator = rational.GetDenominator();
  Rational rational_new(numerator, denominator);
  return rational_new;
}

Rational operator+(const Rational& rational) {
  int32_t numerator = rational.GetNumerator();
  int32_t denominator = rational.GetDenominator();
  Rational rational_new(numerator, denominator);
  return rational_new;
}

Rational& operator--(Rational& rational) {
  int32_t numerator = rational.GetNumerator();
  int32_t denominator = rational.GetDenominator();
  rational.SetNumerator(numerator - denominator);
  rational.NormalRational();
  return rational;
}

Rational& operator++(Rational& rational) {
  int32_t numerator = rational.GetNumerator();
  int32_t denominator = rational.GetDenominator();
  rational.SetNumerator(numerator + denominator);
  rational.NormalRational();
  return rational;
}

Rational operator--(Rational& rational, int) {
  auto copy = rational;
  int32_t numerator = rational.GetNumerator();
  int32_t denominator = rational.GetDenominator();
  rational.SetNumerator(numerator - denominator);
  rational.NormalRational();
  return copy;
}

Rational operator++(Rational& rational, int) {
  auto copy = rational;
  int32_t numerator = rational.GetNumerator();
  int32_t denominator = rational.GetDenominator();
  rational.SetNumerator(numerator + denominator);
  rational.NormalRational();
  return copy;
}

bool operator>(const Rational& lhs, const Rational& rhs) {
  return (lhs.GetNumerator() * rhs.GetDenominator()) > (rhs.GetNumerator() * lhs.GetDenominator());
}

bool operator<(const Rational& lhs, const Rational& rhs) {
  return (lhs.GetNumerator() * rhs.GetDenominator()) < (rhs.GetNumerator() * lhs.GetDenominator());
}

bool operator==(const Rational& lhs, const Rational& rhs) {
  bool result = false;
  if (lhs.GetNumerator() == rhs.GetNumerator() && lhs.GetDenominator() == lhs.GetDenominator()) {
    result = true;
  }
  return result;
}

bool operator!=(const Rational lhs, const Rational rhs) {
  return !(lhs == rhs);
}

bool operator<=(const Rational lhs, const Rational rhs) {
  return !(lhs > rhs);
}

bool operator>=(const Rational lhs, const Rational rhs) {
  return !(lhs < rhs);
}

std::ostream& operator<<(std::ostream& os, const Rational& rational) {
  if (rational.GetDenominator() == 1) {
    return os << rational.GetNumerator();
  }
  return os << rational.GetNumerator() << '/' << rational.GetDenominator();
}

std::istream& operator>>(std::istream& is, Rational& rational) {
  int32_t x = 0;
  int32_t y = 1;
  is >> x;
  if (is.peek() == '/') {
    char slash = 0;
    is >> slash >> y;
  }
  rational = Rational(x, y);
  return is;
}