#ifndef SHARED_PTR_H
#define SHARED_PTR_H

#include <stdexcept>
#define WEAK_PTR_IMPLEMENTED

class BadWeakPtr : public std::runtime_error {
 public:
  BadWeakPtr() : std::runtime_error("BadWeakPtr") {
  }
};

template <class T>
class WeakPtr;

template <class T>
class SharedPtr {
  T* ptr_;
  size_t* counter_;
  size_t* weak_counter_;

  SharedPtr(T* pointer, size_t* counter, size_t* weak_counter)
      : ptr_(pointer), counter_(counter), weak_counter_(weak_counter) {
    ++*counter_;
  }

 public:
  SharedPtr() : ptr_(nullptr), counter_(nullptr), weak_counter_(nullptr) {
  }

  SharedPtr(std::nullptr_t) : ptr_(nullptr), counter_(nullptr), weak_counter_(nullptr) {  // NOLINT
  }

  explicit SharedPtr(T* pointer) : ptr_(pointer), counter_(new size_t{1}), weak_counter_(new size_t{0}) {
  }

  SharedPtr(const SharedPtr& other) : ptr_(other.ptr_), counter_(other.counter_), weak_counter_(other.weak_counter_) {
    if (ptr_ != nullptr) {
      ++*counter_;
    }
  }

  SharedPtr(SharedPtr&& other) noexcept
      : ptr_(other.ptr_), counter_(other.counter_), weak_counter_(other.weak_counter_) {
    other.ptr_ = nullptr;
    other.counter_ = nullptr;
    other.weak_counter_ = nullptr;
  }

  SharedPtr& operator=(SharedPtr&& other) noexcept {
    if (this != &other) {
      SharedPtr(std::move(other)).Swap(*this);
    }
    return *this;
  }

  SharedPtr& operator=(const SharedPtr& other) {
    if (this != &other) {
      SharedPtr(other).Swap(*this);
    }
    return *this;
  }

  ~SharedPtr() {
    if (ptr_ == nullptr) {
      return;
    }
    --*counter_;
    if (*counter_ + *weak_counter_ == 0) {
      delete counter_;
      delete weak_counter_;
      delete ptr_;
    } else if (*counter_ == 0) {
      delete ptr_;
    }
  }

  void Reset(T* ptr) {
    SharedPtr(ptr).Swap(*this);
  }

  void Reset() noexcept {
    SharedPtr().Swap(*this);
  }

  T* Get() const {
    return ptr_;
  }

  size_t UseCount() const {
    if (counter_ == nullptr) {
      return 0;
    }
    return *counter_;
  }

  void Swap(SharedPtr& other) noexcept {
    std::swap(ptr_, other.ptr_);
    std::swap(counter_, other.counter_);
    std::swap(weak_counter_, other.weak_counter_);
  }

  T& operator*() const {
    return *ptr_;
  }

  T* operator->() const {
    return ptr_;
  }

  explicit operator bool() const {
    return ptr_ != nullptr;
  }

  explicit SharedPtr(const WeakPtr<T>& other)
      : ptr_(other.ptr_), counter_(other.counter_), weak_counter_(other.weak_counter_) {
    if (counter_ == nullptr) {
      throw BadWeakPtr{};
    }
    if (*counter_ == 0) {
      throw BadWeakPtr{};
    }
    ++*counter_;
  }

  template <class U>
  friend class WeakPtr;
};

template <class T>
class WeakPtr {
  T* ptr_;
  size_t* counter_;
  size_t* weak_counter_;

 public:
  WeakPtr() : ptr_(nullptr), counter_(nullptr), weak_counter_(nullptr) {
  }

  WeakPtr(const WeakPtr& other) : ptr_(other.ptr_), counter_(other.counter_), weak_counter_(other.weak_counter_) {
    if (counter_ != nullptr) {
      ++*other.weak_counter_;
    }
  }

  WeakPtr& operator=(const WeakPtr<T>& other) {
    if (this != &other) {
      WeakPtr(other).Swap(*this);
    }
    return *this;
  }

  WeakPtr(WeakPtr&& other) noexcept : ptr_(other.ptr_), counter_(other.counter_), weak_counter_(other.weak_counter_) {
    other.ptr_ = nullptr;
    other.counter_ = nullptr;
    other.weak_counter_ = nullptr;
  }

  WeakPtr& operator=(WeakPtr&& other) noexcept {
    if (this != &other) {
      WeakPtr(std::move(other)).Swap(*this);
    }
    return *this;
  }

  WeakPtr(const SharedPtr<T>& other)  // NOLINT explicit
      : ptr_(other.ptr_), counter_(other.counter_), weak_counter_(other.weak_counter_) {
    if (ptr_ != nullptr) {
      ++*weak_counter_;
    }
  }

  ~WeakPtr() {
    if (counter_) {
      --*weak_counter_;
      if (*counter_ + *weak_counter_ == 0) {
        delete counter_;
        delete weak_counter_;
      }
    }
  }

  void Swap(WeakPtr& other) noexcept {
    std::swap(ptr_, other.ptr_);
    std::swap(counter_, other.counter_);
    std::swap(weak_counter_, other.weak_counter_);
  }

  void Reset() noexcept {
    if (weak_counter_ != nullptr) {
      --*weak_counter_;
      ptr_ = nullptr;
      counter_ = nullptr;
      weak_counter_ = nullptr;
      WeakPtr{}.Swap(*this);
    }
  }

  size_t UseCount() const {
    if (counter_ == nullptr) {
      return 0;
    }
    return *counter_;
  }

  bool Expired() const {
    if (counter_ == nullptr) {
      return true;
    }
    return *counter_ == 0;
  }

  SharedPtr<T> Lock() const {
    return Expired() ? SharedPtr<T>() : SharedPtr<T>(ptr_, counter_, weak_counter_);
  }

  WeakPtr& operator=(const SharedPtr<T>& other) {
    WeakPtr(other).Swap(*this);
    return *this;
  }

  template <class U>
  friend class SharedPtr;
};

#endif