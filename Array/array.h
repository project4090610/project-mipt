#ifndef ARRAY_H
#define ARRAY_H

#include <stdexcept>

class ArrayOutOfRange : public std::out_of_range {
 public:
  ArrayOutOfRange() : std::out_of_range("ArrayOutOfRange") {
  }
};

template <class T, size_t N>
struct Array {
  T buffer[N];

  size_t Size() const {
    return N;
  }

  bool Empty() const {
    return N == 0;
  }

  void Fill(const T& value) {
    for (size_t i = 0; i < N; ++i) {
      buffer[i] = value;
    }
  }

  void Swap(Array& other) {
    for (size_t i = 0; i < N; ++i) {
      std::swap(buffer[i], other.buffer[i]);
    }
  }

  T& Front() {
    return buffer[0];
  }

  const T& Front() const {
    return buffer[0];
  }

  T& Back() {
    return buffer[N - 1];
  }

  const T& Back() const {
    return buffer[N - 1];
  }

  const T* Data() const {
    return buffer;
  }

  T* Data() {
    return buffer;
  }

  T& operator[](size_t idx) {
    return buffer[idx];
  }

  const T& operator[](size_t idx) const {
    return buffer[idx];
  }

  T& At(size_t idx) {
    if (idx >= N) {
      throw ArrayOutOfRange{};
    }
    return buffer[idx];
  }

  const T& At(size_t idx) const {
    if (idx >= N) {
      throw ArrayOutOfRange{};
    }
    return buffer[idx];
  }
};

#endif